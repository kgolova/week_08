package ru.edu.servlets;
import org.junit.Before;
import org.junit.Test;
import ru.edu.db.InitDataSource;
import ru.edu.db.CRUD;
import ru.edu.db.Record;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class IndexServletTest {

    public static final String WEB_INF_INDEX_JSP = "/WEB-INF/index.jsp";
    public static final String RECORDS = "records";
    private HttpServletRequest request = mock(HttpServletRequest.class);
    private HttpServletResponse response = mock(HttpServletResponse.class);
    private RequestDispatcher dispatcher = mock(RequestDispatcher.class);
    private DataSource dataSourceMock = mock(DataSource.class);
    private DataSource dataSource;
    private CRUD instance;

    @Before
    public void setupJndi() throws NamingException {

        initDataSource();
        CRUD.setOverride(mock(CRUD.class));
        instance = CRUD.getInstance();

    }


    @Test
    public void test001_doGetTest() throws ServletException, IOException {

        IndexServlet indexServlet = new IndexServlet();
        List<Record> list = new ArrayList<>();
        when(request.getRequestDispatcher(WEB_INF_INDEX_JSP)).thenReturn(dispatcher);
        when(instance.getIndex()).thenReturn(list);
        indexServlet.doGet(request, response);
        verify(request, times(1)).setAttribute(RECORDS, list);
        verify(dispatcher, times(1)).forward(request, response);

    }


    private void initDataSource() throws NamingException {

        InitDataSource initDataSource = new InitDataSource(dataSourceMock);
        dataSource = initDataSource.getDataSource();
        assertEquals(dataSourceMock, dataSource);

    }


}