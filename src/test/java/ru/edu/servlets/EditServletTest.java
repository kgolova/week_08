package ru.edu.servlets;
import org.junit.Before;
import org.junit.Test;
import ru.edu.db.InitDataSource;
import ru.edu.db.CRUD;
import ru.edu.db.Record;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class EditServletTest {

    public static final String RECORD = "record";
    public static final String WEB_INF_EDIT_JSP = "/WEB-INF/edit.jsp";
    public static final String UTF_8 = "UTF-8";
    public static final String ID = "id";
    public static final String VIEW_ID_1 = "view?id=1";
    public static final String EDIT_STATUS_OK_ID_1 = "edit?status=ok&id=0";

    private HttpServletRequest request = mock(HttpServletRequest.class);
    private HttpServletResponse response = mock(HttpServletResponse.class);
    private RequestDispatcher dispatcher= mock(RequestDispatcher.class);
    private Map<String, String[]> map = new HashMap<>();
    private Record recordMock = mock(Record.class);
    private DataSource dataSourceMock = mock(DataSource.class);
    private DataSource dataSource;
    private CRUD instance;

    @Before
    public void setupJndi() throws NamingException {
        init();
        initMap();
        CRUD.setOverride(mock(CRUD.class));
        instance = CRUD.getInstance();
    }


    @Test
    public void test001_doGetIdNotNullTest() throws ServletException, IOException {

        EditServlet editServlet = new EditServlet();
        Record record = new Record();

        when(request.getParameter("id")).thenReturn("1");
        when(request.getRequestDispatcher(WEB_INF_EDIT_JSP)).thenReturn(dispatcher);
        when(instance.getById("1")).thenReturn(record);

        editServlet.doGet(request, response);

        verify(request, times(1)).setAttribute(RECORD, record);
        verify(response, times(1)).setCharacterEncoding(UTF_8);
        verify(dispatcher, times(1)).forward(request, response);

    }


    @Test
    public void test002_doGetIdIsNullTest() throws ServletException, IOException {

        EditServlet editServlet = new EditServlet();

        when(request.getParameter("id")).thenReturn(null);
        when(request.getRequestDispatcher(WEB_INF_EDIT_JSP)).thenReturn(dispatcher);
        editServlet.doGet(request, response);

        verify(request, times(1)).setAttribute(RECORD, null);
        verify(response, times(1)).setCharacterEncoding(UTF_8);
        verify(dispatcher, times(1)).forward(request, response);

    }


    @Test
    public void test003_doPostTitleIsNullTest() throws ServletException, IOException {

        EditServlet editServlet = new EditServlet();
        map.put("title", new String[]{null});
        when(request.getParameterMap()).thenReturn(map);
        when(recordMock.getTitle()).thenReturn(null);
        editServlet.doPost(request, response);
        verify(response, times(1)).sendRedirect(VIEW_ID_1);

    }


    @Test
    public void test004_doPostTitleIsNotNullTest() throws ServletException, IOException {


        EditServlet editServlet = new EditServlet();
        map.put("title", new String[]{"title"});

        when(request.getParameterMap()).thenReturn(map);
        when(recordMock.getTitle()).thenReturn(null);
        editServlet.doPost(request, response);
        verify(response, times(1)).sendRedirect(EDIT_STATUS_OK_ID_1);

    }


    private void init() throws NamingException {

        InitDataSource initDataSource = new InitDataSource(dataSourceMock);
        dataSource = initDataSource.getDataSource();
        assertEquals(dataSourceMock, dataSource);

    }


    private void initMap() {

        map.put("id", new String[]{"1"});
        map.put("type", new String[]{"type"});
        map.put("text", new String[]{"text"});
        map.put("price", new String[]{"1"});
        map.put("publisher", new String[]{"publisher"});
        map.put("email", new String[]{"email"});
        map.put("phone", new String[]{"phone"});
        map.put("picture_url", new String[]{"picture_url"});

    }


}