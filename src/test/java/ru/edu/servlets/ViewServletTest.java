package ru.edu.servlets;
import org.junit.Before;
import org.junit.Test;
import ru.edu.db.InitDataSource;
import ru.edu.db.CRUD;
import ru.edu.db.Record;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class ViewServletTest {

    public static final String RECORD = "record";
    public static final String INDEX_ERROR_ID_PARAM_MISSING = "/index?error=id_param_missing";
    public static final String INDEX_ERROR_ID_NOT_FOUND = "/index?error=id_not_found";
    public static final String WEB_INF_VIEW_JSP = "/WEB-INF/view.jsp";

    private HttpServletRequest request = mock(HttpServletRequest.class);
    private HttpServletResponse response = mock(HttpServletResponse.class);
    private RequestDispatcher dispatcher= mock(RequestDispatcher.class);

    private DataSource dataSourceMock = mock(DataSource.class);
    private DataSource dataSource;

    private CRUD instance;


    @Before
    public void setupJndi() throws NamingException {

        initDataSource();
        CRUD.setOverride(mock(CRUD.class));
        instance = CRUD.getInstance();

    }


    @Test
    public void test001_doGetIdNullTest() throws ServletException, IOException {

        ViewServlet viewServlet = new ViewServlet();
        when(request.getParameter("id")).thenReturn(null);

        viewServlet.doGet(request, response);
        verify(response, times(1)).sendRedirect(INDEX_ERROR_ID_PARAM_MISSING);

    }


    @Test
    public void test002_doGetRecordNullTest() throws ServletException, IOException {

        ViewServlet viewServlet = new ViewServlet();
        when(request.getParameter("id")).thenReturn("1");
        when(instance.getById("1")).thenReturn(null);
        viewServlet.doGet(request, response);
        verify(response, times(1)).sendRedirect(INDEX_ERROR_ID_NOT_FOUND);

    }


    @Test
    public void test003_doGetRecordNotNullTest() throws ServletException, IOException {

        ViewServlet viewServlet = new ViewServlet();
        Record record = new Record();

        when(request.getParameter("id")).thenReturn("1");
        when(instance.getById("1")).thenReturn(record);
        when(request.getRequestDispatcher(WEB_INF_VIEW_JSP)).thenReturn(dispatcher);

        viewServlet.doGet(request, response);
        verify(request, times(1)).setAttribute(RECORD, record);
        verify(dispatcher, times(1)).forward(request, response);

    }


    private void initDataSource() throws NamingException {

        InitDataSource initDataSource = new InitDataSource(dataSourceMock);
        dataSource = initDataSource.getDataSource();
        assertEquals(dataSourceMock, dataSource);

    }


}