package ru.edu.db;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;


public class CRUD {

    /**
     * Выбрать все.
     */
    public static final String SELECT_ALL_SQL =
            "SELECT * FROM records";

    /**
     * Выбрать запись по ID.
     */
    public static final String SELECT_BY_ID =
            "SELECT * FROM records WHERE id=?";

    /**
     * Добавить запись по таблицу.
     */
    public static final String INSERT_SQL =
            "INSERT INTO records (title, type, text, price, "
                    + "publisher, email, phone, picture_url) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

    /**
     * Обновить запись по таблице.
     */
    public static final String UPDATE_SQL =
            "UPDATE records SET title=?, type=?, text=?, price=?, "
                    + "publisher=?, email=?, phone=?, picture_url=? WHERE id=?";

    /**
     * Экземпляр CRUD.
     */
    private static CRUD instance;

    /**
     * Источник.
     */
    private DataSource dataSource;

    /**
     * CRUD override.
     */
    private static CRUD override;

    /**
     * Конструтор.
     * @param dataSourcetmp
     */
    public CRUD(final DataSource dataSourcetmp) {
        this.dataSource = dataSourcetmp;
    }

    /**
     * Синглтон.
     *
     * @return CRUD
     */
    public static CRUD getInstance() {
        synchronized (CRUD.class) {
            if (override != null) {
                return override;
            }
            if (instance == null) {
                try {
                    Context ctx = new InitialContext();
                    Context env = (Context) ctx.lookup("java:/comp/env");
                    DataSource dataSource =
                            (DataSource) env.lookup("jdbc/dbLink");
                    instance = new CRUD(dataSource);
                } catch (Exception e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
        }
        return instance;
    }

    /**
     * Получить записи.
     * @return List<Record>
     */
    public List<Record> getIndex() {
        return query(SELECT_ALL_SQL);
    }

    /**
     * Запрос.
     * @param sql
     * @param values
     * @return List<Record>
     */
    private List<Record> query(final String sql, final Object... values) {
        try (PreparedStatement statement =
                     getConnection().prepareStatement(sql)) {
            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }
            ResultSet resultSet = statement.executeQuery();
            List<Record> result = new ArrayList<>();
            while (resultSet.next()) {
                result.add(map(resultSet));
            }
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Записать.
     * @param resultSet
     * @return Record
     * @throws SQLException
     */
    private Record map(final ResultSet resultSet) throws SQLException {

        Record record = new Record();
        record.setId(resultSet.getLong("id"));
        record.setTitle(resultSet.getString("title"));
        record.setType(resultSet.getString("type"));
        record.setPrice((resultSet.getString("price") == null)
                ? new BigDecimal(0)
                : new BigDecimal(resultSet.getString("price")));
        record.setText(resultSet.getString("text"));
        record.setPublisher(resultSet.getString("publisher"));
        record.setEmail(resultSet.getString("email"));
        record.setPhone(resultSet.getString("phone"));
        record.setPictureUrl(resultSet.getString("picture_url"));
        return record;

    }


    /**
     * Получить соединение.
     * @return Connection
     */
    private Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }

    }

    /**
     * Получить запись по ID.
     * @param id
     * @return Record
     */
    public Record getById(final String id) {
        List<Record> records = query(SELECT_BY_ID, id);
        if (records.isEmpty()) {
            return null;
        }
        return records.get(0);
    }

    /**
     * Выполнить запрос.
     * @param sql
     * @param values
     * @return ID
     */
    private Long  execute(final String sql, final Object... values) {
        try (PreparedStatement statement =
                     getConnection().prepareStatement(sql,
                             Statement.RETURN_GENERATED_KEYS)) {
            for (int i = 0; i < values.length; i++) {

                statement.setObject(i + 1, values[i]);
            }
            Long id = null;

            statement.executeUpdate();

            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getLong(1);
            }
            return id;

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

    /**
     * Сохранить запись.
     * @param record
     * @return ID
     */
    public Long save(final Record record) {

        Long id;

        if (record.getId() == null) {
            id = insert(record);

        } else {
            id = update(record);
        }
        return id;
    }


    /**
     * Обновить запись.
     * @param record
     * @return ID
     */
    public Long update(final Record record) {
        Long id = execute(UPDATE_SQL, record.getTitle(), record.getType(),
                record.getText(), (record.getPrice() == null)
                        ? new BigDecimal(0)
                        : record.getPrice().toPlainString(),
                record.getPublisher(),
                record.getEmail(), record.getPhone(), record.getPictureUrl(),
                record.getId());
        return id;
    }


    /**
     * Добавить запись.
     * @param record
     * @return ID
     */
    public Long insert(final Record record) {

        Long id = execute(INSERT_SQL, record.getTitle(), record.getType(),
                record.getText(), (record.getPrice() == null)
                        ? new BigDecimal(0) : record.getPrice().toPlainString(),
                record.getPublisher(), record.getEmail(), record.getPhone(),
                record.getPictureUrl());
        return id;
    }

    /**

     * getOverride.
     * @return - override
     */
    public static CRUD getOverride() {
        return override;
    }


    /**
     * setOverride.
     * @param anOverride - anOverride
     */

    public static void setOverride(final CRUD anOverride) {
        CRUD.override = anOverride;
    }

}

