package ru.edu.db;

import java.math.BigDecimal;

public class Record {

    /**
     * ID.
     */
    private Long id;

    /**
     * Заголовок.
     */
    private String title;

    /**
     * Тип услуги.
     */
    private String type;

    /**
     * Цена.
     */
    private BigDecimal price;

    /**
     * Текст.
     */
    private String text;

    /**
     * Автор.
     */
    private String publisher;

    /**
     * e-mail.
     */
    private String email;

    /**
     * Телефон.
     */
    private String phone;

    /**
     * url картинки.
     */
    private String pictureUrl;

    /**
     * getId.
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * Установить ID.
     * @param idtmp
     */
    public void setId(final Long idtmp) {
        this.id = idtmp;
    }

    /**
     * getTitle.
     * @return title
     */
    public String getTitle() {
        return title;
    }


    /**
     * Установить заголовок.
     * @param titletmp
     */
    public void setTitle(final String titletmp) {
        this.title = titletmp;
    }


    /**
     * Получить тип.
     * @return type
     */
    public String getType() {
        return type;
    }

    /**
     * Установить тип.
     * @param typemp
     */
    public void setType(final String typemp) {
        this.type = typemp;
    }

    /**
     * Получить цену.
     * @return price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Установить цену.
     * @param pricetmp
     */
    public void setPrice(final BigDecimal pricetmp) {
        this.price = pricetmp;
    }

    /**
     * Получить текст.
     * @return text
     */
    public String getText() {
        return text;
    }

    /**
     * Установить текст.
     * @param texttmp
     */
    public void setText(final String texttmp) {
        this.text = texttmp;
    }

    /**
     * Получить автора.
     * @return publisher
     */
    public String getPublisher() {
        return publisher;
    }

    /**
     * Установить автора.
     * @param publishertmp
     */
    public void setPublisher(final String publishertmp) {
        this.publisher = publishertmp;
    }

    /**
     * Получить e-mail.
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Установить e-mail.
     * @param emailtmp
     */
    public void setEmail(final String emailtmp) {
        this.email = emailtmp;
    }

    /**
     * Получить номер.
     * @return phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Установить номер.
     * @param phonetmp
     */
    public void setPhone(final String phonetmp) {
        this.phone = phonetmp;
    }

    /**
     * Получить url картинки.
     * @return pictureUrl
     */
    public String getPictureUrl() {
        return pictureUrl;
    }

    /**
     * Установить url картинки.
     * @param pictureUrltmp
     */
    public void setPictureUrl(final String pictureUrltmp) {
        this.pictureUrl = pictureUrltmp;
    }

    /**
     * Получить короткое представление.
     * @param length
     * @return text
     */
    public String getShort(final int length) {
        if (text.length() <= length) {
            return text;
        }
        return text.substring(0, length) + "...";
    }

}

